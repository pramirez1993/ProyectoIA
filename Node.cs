﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechR
{
    class Node
    {
        // Suerce A* algorithm: https://www.youtube.com/watch?v=KNXfSOx4eEE&t=285s

        // H Value: Heuristic 
        private int _h_value;
        // G Value: movement cost
        private int _g_value;
        // F Value: H Value + G Value
        private int _f_value;
        // Parent: parent assigned to this node to reach this node
        private Node _father;
        // Value: the position value of a node in the Matrix 
        private int _value;
        // Position of the Node in the matrix
        private int _x_position;
        private int _y_position;
        // id for the Node
        int _id;

        //static int M;
        //static int N;
        // Matrix of MxN
        //Node[,] Matrix = new Node[M1, N1];


        public int h_value
        {
            get
            {
                return _h_value;
            }
            set
            {
                _h_value = value;
            }
        }

        public int g_value
        {
            get
            {
                return _g_value;
            }
            set
            {
                _g_value = value;
            }
        }

        public int f_value
        {
            get
            {
                return _f_value;
            }
            set
            {
                _f_value = value;
            }
        }

        public int x_position
        {
            get
            {
                return _x_position;
            }
            set
            {
                _x_position = value;
            }
        }

        public int y_position
        {
            get
            {
                return _y_position;
            }
            set
            {
                _y_position = value;
            }
        }

        public int value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        //internal Node[,] Matrix1 { get => Matrix; set => Matrix = value; }
        //public int M1 { get => M; set => M = value; }
        //public static int N1 { get => N; set => N = value; }
        internal Node Father { get => _father; set => _father = value; }
        public int Id { get => _id; set => _id = value; }
        
    } // End of Class Node
}
