﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manhattan
{
    class SquaresLabyrinth
    {
        private int _n;
        private int _m;
        private int _a;
        private CellSpace[][] _board;

        public SquaresLabyrinth(int n, int m , int a)
        {
            _n = n;
            _m = m;
            _a = a;
            _board = new CellSpace[_n][];
            _board = initCellSpace(_board);
        }

        public int n
        {
            get
            {
                return _n;
            }

            set
            {
                _n = value;
            }
        }

        public int m
        {
            get
            {
                return _m;
            }
            set
            {
                _m = value;
            }
        }

        public int a
        {
            get
            {
                return _a;
            }
            set
            {
                _a = value;
            }
        }

        public CellSpace[][] board
        {
            get
            {
                return _board;
            }
            set
            {
                _board = value;
            }
        }

        /*Inicialización del array de arrays para que todo sea new*/
        public CellSpace[][] initCellSpace(CellSpace[][] board)
        {
            for (int i = 0; i < board.Length; i++)
            {
                board[i] = new CellSpace[_m];
                for (int j = 0; j < board[i].Length; j++)
                {
                    board[i][j] = new CellSpace();
                }
            }
            return board;
        }
    }
}
