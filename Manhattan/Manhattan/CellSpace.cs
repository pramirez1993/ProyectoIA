﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manhattan
{
    class CellSpace
    {
        private int _distance;
        private Boolean _obstacle;
        private Boolean _visited;/*Visited se pasa a true cuando ya se le puso distance a un espacio*/

        public Boolean visited
        {
            get
            {
                return _visited;
            }

            set
            {
                _visited = value;
            }
        }

        public int distance
        {
            get
            {
                return _distance;
            }

            set
            {
                _distance = value;
            }
        }

        public Boolean obstacle
        {
            get
            {
                return _obstacle;
            }

            set
            {
                _obstacle = value;
            }
        }
    }
}
