﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manhattan
{
    class Node
    {
        private int _h_value;
        private int _g_value;
        private int _f_value;
        private int _x_position;
        private int _y_position;
        private int _value;

        public int h_value
        {
            get
            {
                return _h_value;
            }
            set
            {
                _h_value = value;
            }
        }

        public int g_value
        {
            get
            {
                return _g_value;
            }
            set
            {
                _g_value = value;
            }
        }

        public int f_value
        {
            get
            {
                return _f_value;
            }
            set
            {
                _f_value = value;
            }
        }

        public int x_position
        {
            get
            {
                return _x_position;
            }
            set
            {
                _x_position = value;
            }
        }

        public int y_position
        {
            get
            {
                return _y_position;
            }
            set
            {
                _y_position = value;
            }
        }

        public int value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
    }
}
