﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manhattan
{
    class GameSetUp
    {
        private int[][] _board;
        private int _width;
        private int _height;
        private int _probability;

        public GameSetUp(int height, int width, int a, int probability)
        {
            this._width = width;
            this._height = height;
            this._probability = probability;
            this._board = init_board(height, width);
            this.board = set_obstacles(_board, _probability);
            this.board = set_available_spaces(_board);
        }

        public int[][] init_board(int width, int height)
        {
            /*
             * 
             */
            int[][] board = new int[height][];
            for (int i = 0; i < board.Length; i++)
            {
                board[i] = new int[width];
            }
            return board;
        }

        private int[][] set_obstacles(int[][] board, int probability)
        {
            Random rnd = new Random();
            int prob = 0;
            for (int i = 0; i < board.Length; i++)
            {
                for (int j = 0; j < board[i].Length; j++)
                {
                    prob = rnd.Next(0, 100);
                    if (prob <= probability)
                    {
                        board[i][j] = 2;
                    }
                }
            }
            return board;
        }

        public int[][] set_start_or_destiny(int[][] board_attr, int start_or_destiny)
        {
            Random rnd = new Random();
            int[][] board = board_attr;
            int pos_y = rnd.Next(0, (_height-1));
            int pos_x = rnd.Next(0, (_width-1));
            if ((board[pos_x][pos_y] == 2) | (board[pos_x][pos_y] == 0))
            {
                set_start_or_destiny(board, start_or_destiny);
            }            
            return board;
        }

        public int[][] set_available_spaces(int[][] board_attr)
        {
            int[][] board = board_attr;
            for (int i = 0; i < board.Length; i++)
            {
                for (int j = 0; j < board[i].Length; j++)
                {
                    if ((board[i][j] != 2) & (board[i][j] != 0) & (board[i][j] != 3))
                    {
                        board[i][j] = 1;
                    }
                }
            }
            board = set_start_or_destiny(board, 0);
            board = set_start_or_destiny(board, 3);
            return board;
        }

        public int[][] board
        {
            get
            {
                return _board;
            }
            set
            {
                _board = value;
            }
        }

        public int width
        {
            get
            {
                return _width;
            }
            set
            {
                _width = value;
            }
        }

        public int height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }

        public int probability
        {
            get
            {
                return _probability;
            }
            set
            {
                _probability = value;
            }
        }
    }
}
