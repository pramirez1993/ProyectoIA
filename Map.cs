﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeechR
{
    class Map
    {
        private Node[][] _matrix;
        private bool _walk_diagonally;

  
        // Functions

        // Name: What this function DO
        // Parameters:
        // Return: 


        // aStar: Returns the shortest path between 2 given points in a M*N Matrix
        // Parameters: Node x and Node y, x = Agent Position and y = Destination
        // Return: Shortest path list of nodes
        public List<Node> AStar(Node _initialNode, Node _destinationNode)
        {
            Node initialNode = new Node();
            List<Node> path_list = new List<Node>(); // Answer
            initialNode.Father = null;
            // Open List
            List<Node> adjacentList = new List<Node>();
            List<Node> openList = new List<Node>();
            List<Node> closeList = new List<Node>();
            List<Node> pathList = new List<Node>();
            Node temp = new Node();



            return path_list;
        }

        // Name: What this function DO
        // Parameters:
        // Return: 
        private List<Node> getAdjacentNodes(Node pNode)
        {
            List<Node> answer = new List<Node>();

            int x_coordinate = pNode.x_position;
            int y_coordinate = pNode.y_position;

            // Verificar Atras
            if (_matrix[x_coordinate][y_coordinate].y_position -1 >= 0) // Osea si es un campo valido de la matriz el que voy a consultar
            {
                   // Ya se que es un campo dentro de la matriz
                   // Ahora chequeo que ese nodo no este bloqueado
                if (_matrix[x_coordinate][y_coordinate -1].value != 2)
                {
                    _matrix[x_coordinate][y_coordinate - 1].g_value = 10; // 10 es el costo del padre al moverse a este nodo
                    _matrix[x_coordinate][y_coordinate - 1].f_value = 
                        _matrix[x_coordinate][y_coordinate - 1].h_value +
                        _matrix[x_coordinate][y_coordinate - 1].g_value;

                    _matrix[x_coordinate][y_coordinate - 1].Father = pNode; // Ponemos de padre al Node que viene por parametro
                    answer.Add(_matrix[x_coordinate][y_coordinate - 1]); // Lo agregamos a la lista de adyacentes
                }

                // Ahora adyacentes en diagonal
                // El costo de llegar a estos nodos desde el padre es de: 14
                if(_walk_diagonally == true)
                {
                    Console.WriteLine("Diagonal Movement ON");
                }
                else { Console.WriteLine("Diagonal Movement OFF");  }

            }


            /*
            // Verificar Arriba primero
            if (_matrix[x_coordinate - 1][y_coordinate].value != 2) // Osea si no es un campo bloqueado
            {
                this.matriz[x - 1][y].set_GValor(10);
                this.matriz[x - 1][y].set_FValor(this.matriz[x - 1][y].get_HValor() + this.matriz[x - 1][y].get_GValor());
                this.matriz[x - 1][y].set_Padre(pNodo);
                answer.add(this.matriz[x - 1][y]);
            }
            
            // Verificar Delante
            if (this.matriz[x][y + 1].get_EsPasable())
            {
                this.matriz[x][y + 1].set_GValor(10);
                this.matriz[x][y + 1].set_FValor(this.matriz[x][y + 1].get_HValor() + this.matriz[x][y + 1].get_GValor());
                this.matriz[x][y + 1].set_Padre(pNodo);
                answer.add(this.matriz[x][y + 1]);
            }
            // Verificar Abajo
            if (this.matriz[x + 1][y].get_EsPasable())
            {
                this.matriz[x + 1][y].set_GValor(10);
                this.matriz[x + 1][y].set_FValor(this.matriz[x + 1][y].get_HValor() + this.matriz[x + 1][y].get_GValor());
                this.matriz[x + 1][y].set_Padre(pNodo);
                answer.add(this.matriz[x + 1][y]);
            }
            */

            return answer;
        }

        // Name: What this function DO
        // Parameters:
        // Return: 
        private void setValues()
        {
            Console.WriteLine("Function to set values...");
        }

        // Name: What this function DO
        // Parameters:
        // Return:
        private Node[][] preCalculateHeuristic(int pRow, int pCol)
        {
            int g_value = 0;

            for (int row = 0; row < _matrix.Length; row++)
            {
                for(int col = 0; col < _matrix[row].Length; col++)
                {
                    g_value = calculateHValue(row, col, pRow, pCol);
                    _matrix[row][col].h_value = g_value;
                }
            }

            return _matrix;
        }

        // Name: What this function DO
        // Parameters:
        // Return: 
        private int calculateHValue(int _initialRow, int _initialCol, int pRow, int pCol)
        {
            int rowDif, colDif = 0;
            int answer = 0;

            rowDif = Math.Abs(_initialRow - pRow);
            colDif = Math.Abs(_initialCol - pCol);
            answer = rowDif + colDif;

            return answer;
        }

        // Name: What this function DO
        // Parameters:
        // Return: 

    } // End of class Map
}
